; Use this file to build a full distribution including Drupal core and the
; Commerce Downloads install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"

; Add Commerce Downloads to the full distribution build.
projects[commerce_downloads][type] = profile
projects[commerce_downloads][download][type] = git
projects[commerce_downloads][download][url] = git://github.com/cgthill/commerce_downloads_profile.git