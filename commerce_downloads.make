api = 2
core = 7.x

; Dependencies =================================================================

projects[addressfield][type] = module
projects[addressfield][subdir] = contrib
projects[addressfield][version] = 1.x-dev

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.x-dev

projects[entity][type] = module
projects[entity][subdir] = contrib
projects[entity][version] = 1.x-dev

projects[rules][type] = module
projects[rules][subdir] = contrib
projects[rules][version] = 2.x-dev

projects[views][type] = module
projects[views][subdir] = contrib
projects[views][version] = 3.x-dev

projects[features][type] = module
projects[features][subdir] = contrib
projects[features][version] = 1.x-dev

projects[commerce_features][type] = module
projects[commerce_features][subdir] = contrib
projects[commerce_features][version] = 1.x-dev

projects[commerce_downloads_feature][type] = module
projects[commerce_downloads_feature][subdir] = feature
projects[commerce_downloads_feature][download][type] = git
projects[commerce_downloads_feature][download][url] = git://github.com/cgthill/commerce_downloads_feature.git

; Drupal Commerce and Commerce contribs ========================================

projects[commerce][type] = module
projects[commerce][subdir] = contrib
projects[commerce][version] = 1.x-dev

projects[commerce_paypal][type] = module
projects[commerce_paypal][subdir] = contrib
projects[commerce_paypal][version] = 1.x-dev

projects[commerce_file][type] = module
projects[commerce_file][subdir] = contrib
projects[commerce_file][version] = 1.x-dev

; themes ========================================

projects[corolla][type] = theme
projects[corolla][subdir] = theme
projects[corolla][version] = 1.21

